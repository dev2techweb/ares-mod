<?php
/**
 * The template used for displaying page content in template-homepage.php
 *
 * @package storefront
 */

?>
<?php
$featured_image = get_the_post_thumbnail_url( get_the_ID(), 'thumbnail' );
?>

<div id="post-<?php the_ID(); ?>" <?php post_class(); ?> style="<?php storefront_homepage_content_styles(); ?>"
	data-featured-image="<?php echo esc_url( $featured_image ); ?>">
	<div class="col-full">
		<?php
		/**
		 * Functions hooked in to storefront_page add_action
		 *
		 * @hooked storefront_homepage_header      - 10
		 * @hooked storefront_page_content         - 20
		 */
		// do_action( 'storefront_homepage' );
		?>
	</div>
</div><!-- #post-## -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
	integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
</script>

<div class="contenido">
	<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img class="d-block w-100" src="<?php echo get_template_directory_uri(); ?>/assets/images/BANNER1.png" alt="First slide">
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="<?php echo get_template_directory_uri(); ?>/assets/images/banner2.png" alt="Second slide">
    </div>
  </div>
  <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>

<table class="table">
  <thead>
    <tr>
	  <td><img src="<?php echo get_template_directory_uri(); ?>/assets/images/exportado-22.png" alt=""></td>
	  <td><img src="<?php echo get_template_directory_uri(); ?>/assets/images/exportado-25.png" alt=""></td>
	  <td><img src="<?php echo get_template_directory_uri(); ?>/assets/images/exportado-26.png" alt=""></td>
	  <td><img src="<?php echo get_template_directory_uri(); ?>/assets/images/exportado-15.png" alt=""></td>
    </tr>
  </thead>
  <tbody>
    <tr>
	 <td><img src="<?php echo get_template_directory_uri(); ?>/assets/images/exportado-14.png" alt=""></td>
	  <td><img src="<?php echo get_template_directory_uri(); ?>/assets/images/exportado-11.png" alt=""></td>
	  <td><img src="<?php echo get_template_directory_uri(); ?>/assets/images/exportado-05.png" alt=""></td>
	  <td><img src="<?php echo get_template_directory_uri(); ?>/assets/images/exportado-07.png" alt=""></td>
    </tr>
    <tr>
	  <td><img src="<?php echo get_template_directory_uri(); ?>/assets/images/exportado-23.png" alt=""></td>
	  <td><img src="<?php echo get_template_directory_uri(); ?>/assets/images/exportado-13.png" alt=""></td>
	  <td><img src="<?php echo get_template_directory_uri(); ?>/assets/images/exportado-24.png" alt=""></td>
	  <td><img src="<?php echo get_template_directory_uri(); ?>/assets/images/exportado-08.png" alt=""></td>
    </tr>
    <tr>
	  <td><img src="<?php echo get_template_directory_uri(); ?>/assets/images/exportado-01.png" alt=""></td>
	  <td><img src="<?php echo get_template_directory_uri(); ?>/assets/images/exportado-12.png" alt=""></td>
	  <td><img src="<?php echo get_template_directory_uri(); ?>/assets/images/exportado-10.png" alt=""></td>
	  <td><img src="<?php echo get_template_directory_uri(); ?>/assets/images/exportado-17.png" alt=""></td>
    </tr>


	<tr>
	  <td><img src="<?php echo get_template_directory_uri(); ?>/assets/images/exportado-02.png" alt=""></td>
	  <td><img src="<?php echo get_template_directory_uri(); ?>/assets/images/exportado-20.png" alt=""></td>
	  <td><img src="<?php echo get_template_directory_uri(); ?>/assets/images/exportado-19.png" alt=""></td>

	  <td><img src="<?php echo get_template_directory_uri(); ?>/assets/images/exportado-03.png" alt=""></td>
    </tr>


	<tr>
	  <td><img src="<?php echo get_template_directory_uri(); ?>/assets/images/exportado-09.png" alt=""></td>
	  <td><img src="<?php echo get_template_directory_uri(); ?>/assets/images/exportado-04.png" alt=""></td>

	  <td><img src="<?php echo get_template_directory_uri(); ?>/assets/images/exportado-18.png" alt=""></td>
	  <td><img src="<?php echo get_template_directory_uri(); ?>/assets/images/exportado-27.png" alt=""></td>
    </tr>


	<tr>
	<td><img src="<?php echo get_template_directory_uri(); ?>/assets/images/exportado-28.png" alt=""></td>

	<td><img src="<?php echo get_template_directory_uri(); ?>/assets/images/exportado-16.png" alt=""></td>
    </tr>
  </tbody>
</table>



	  
	  
	  

<div class="clase">
	<hr>
</div>
	<section id="productos">
		<div class="title__section">
		
			<img src="<?php echo get_template_directory_uri(); ?>/assets/images/exportado-35.png" alt="Nuevos Ingresos">
  			<div class="centrado">Nuevos Ingresos</div>
		</div>
		<!-- productos -->
		<div class="productos">
		<?php echo do_shortcode('[wpb-product-slider autoplay="true" loop="true" nav="true" pagination="true" items="4" items_desktop_small="3" items_tablet="2" items_mobile="2" posts="12"]');?>
		</div>
		</div>
	</section>
	
	<section id="nuevasofertas">
	<div class="title__section">
			<img src="<?php echo get_template_directory_uri(); ?>/assets/images/exportado-35.png" alt="Nuevos Ingresos">
  			<div class="centrado">En Oferta</div>
			</div>
		<!-- productos -->
		<div class="productos">
		<?php echo do_shortcode('[wpb-product-slider product_type="category" category="18" autoplay="true" loop="true" nav="true" pagination="true" items="4" items_desktop_small="3" items_tablet="2" items_mobile="2" posts="12"]');?>
		
	</div>
	</div>
	</section>
	

